# Seinfield Theme Song Music on Demand
A simple and easy way to get the Seinfield Theme Song on demand.

# Why?
As the successor to Jeopardy Music on Demand, I felt it necessary to make this script. I, sadly, have ILTTMSTMD, commonly known as I Listen To Too Much Seinfield Theme Music Disease. Side effects may include, the excessive need to listen to Seinfield Theme Music.

I needed to make this script to cure this problem.

# Libraries?
PyGame and appJar.

# Yo dawg? Where dem sound flies at?
* 1 hour - https://www.youtube.com/watch?v=KZACorHeE-c
* 10 hours - https://www.youtube.com/watch?v=j3Kgf_dEWjE

Name them this:
* 1 hour - 1hour.mp3
* 10 hours - 10hours.mp3

To convert, use a mp3 converting site. youtube2mp3.cc is what I still use. listentoyoutube.com seems to work best for the 10 hour long file, but it takes at least 20 minutes to run the conversion.

(happy april fools!)
